<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "quiz";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// sql to create table
$sql = "SELECT
costumers.`name` AS costumer_name,
Sum(orders.amount) AS total_amount
FROM
orders
INNER JOIN costumers ON orders.costumer_id = costumers.id
GROUP BY
costumers.`name`
ORDER BY
costumers.id ASC";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo  " - costumer_name: " . $row["costumer_name"]. "<br>-total_amount: " . $row["total_amount"]. "<br><br>";
  }
} else {
  echo "0 results";
}


$conn->close();
?>